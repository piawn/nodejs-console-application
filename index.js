const readline = require('readline');
const PORT = 3000;

getUserInput();

function getUserInput(){
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    
    console.log('Choose an option: ')
    console.log('1. Read package.json');
    console.log('2. Display OS info');
    console.log('3. Start HTTP server');
    
    rl.question('Type a number: ', (answer) => {
        if (answer == 1){
            readPackage();
        } else if (answer == 2){
            displayOSInfo();
        } else if (answer == 3){
            startHTTPServer();
        } else {
            console.log('Invalid input. Valid input is 1, 2 or 3.');
        }
        rl.close();
    });
}

function readPackage(){
    console.log('Reading package.json file...');

    const fs = require('fs');

    fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
        console.log(content);
    });
};

function displayOSInfo(){
    console.log('Getting OS info...');

    const os = require('os');

    let systemMemory = (os.totalmem() / 1024 / 1024 / 1024).toFixed(2);
    let freeMemory = (os.freemem() / 1024 / 1024 / 1024).toFixed(2);
    let cpuCores = os.cpus();
    let arch = os.arch();
    let platform = os.platform();
    let release = os.release();
    let user = os.userInfo().username;

    console.log(`SYSTEM MEMORY: ${systemMemory} GB`);
    console.log(`FREE MEMORY: ${freeMemory} GB`);
    console.log(`CPU CORES: ${cpuCores.length}`);
    console.log(`ARCH: ${arch}`);
    console.log(`PLATFORM: ${platform}`);
    console.log(`RELEASE: ${release}`);
    console.log(`USER: ${user}`);
};

function startHTTPServer(){
    console.log('Starting HTTP server...');

    const http = require('http');
    const server = http.createServer((req, res) => {
        try {
            if (req.url === '/'){
                res.write('Hello World!');
                res.end();
            }
        } catch (e) {
            console.log(e);
            return res.end( e.message );    
        }
    });

    server.listen(PORT, ()=>{
        console.log(`Listening on port ${PORT}...`);
    });
};